package tutorialIsland;
import org.tbot.client.Widget;
import org.tbot.internal.AbstractScript;
import org.tbot.internal.Manifest;
import org.tbot.internal.handlers.LogHandler;
import org.tbot.methods.*;
import org.tbot.methods.Widgets;
import org.tbot.wrappers.WidgetChild;

public class Variables {
    //OBJECT IDS ETC
    //CHARACTER CREATION WIDGETS
    public static final WidgetChild HEAD_CHILD = Widgets.getWidget(269,113);
    public static final WidgetChild JAW_CHILD = Widgets.getWidget(269,114);
    public static final WidgetChild TORSO_CHILD = Widgets.getWidget(269,115);
    public static final WidgetChild ARMS_CHILD = Widgets.getWidget(269,116);
    public static final WidgetChild HANDS_CHILD = Widgets.getWidget(269,117);
    public static final WidgetChild LEGS_CHILD = Widgets.getWidget(269,118);
    public static final WidgetChild FEET_CHILD = Widgets.getWidget(269,119);
    public static final WidgetChild HAIR_CHILD = Widgets.getWidget(269,121);
    public static final WidgetChild TORSO_COLOUR_CHILD = Widgets.getWidget(269,127);
    public static final WidgetChild LEGS_COLOUR_CHILD = Widgets.getWidget(269,127);
    public static final WidgetChild FEET_COLOUR_CHILD = Widgets.getWidget(269,129);
    public static final WidgetChild SKIN_COLOUR_CHILD = Widgets.getWidget(269,129);
    public static final WidgetChild FEMALE_CHILD = Widgets.getWidget(269,137);
    public static final WidgetChild ACCEPT_CHILD = Widgets.getWidget(269,100);
    public static final WidgetChild MASTER_CHILD = Widgets.getWidget(269,0);

    //INTERFACE TABS
    public static final WidgetChild TOOLS = Widgets.getWidget(548,29);
    public static final WidgetChild INVENTORY = Widgets.getWidget(548,45);
    public static final WidgetChild LEVELS = Widgets.getWidget(548,43);
    public static final WidgetChild MUSIC = Widgets.getWidget(548,31);
    public static final WidgetChild EMOTES = Widgets.getWidget(548,30);
    public static final WidgetChild EQUIPMENT = Widgets.getWidget(548,42);
    public static final WidgetChild PRAYER = Widgets.getWidget(548,47);
    public static final WidgetChild FRIENDS = Widgets.getWidget(548,26);
    public static final WidgetChild IGNORE = Widgets.getWidget(548,27);
    public static final WidgetChild MAGIC = Widgets.getWidget(548,48);

    //CHAT BOX
    public static final WidgetChild CONTINUE = Widgets.getWidget(231,2);
    public static final WidgetChild CONTINUE2 = Widgets.getWidget(11,3);
    public static final WidgetChild CONTINUE3 = Widgets.getWidget(217,2);
    public static final WidgetChild CONTINUE4 = Widgets.getWidget(163,32);
    public static final WidgetChild CONTINUE5 = Widgets.getWidget(162,33);
    public static final WidgetChild CONTINUE6 = Widgets.getWidget(229,1);
    public static final WidgetChild CONTINUE7 = Widgets.getWidget(193,2);
    public static final WidgetChild YESBANK = Widgets.getWidget(219,0);


    //SMITHING
    public static final WidgetChild SMITHING = Widgets.getWidget(312,2);


    //RUN
    public static final WidgetChild RUNTAB = Widgets.getWidget(160, 21);

    //EQUIPMENT
    public static final WidgetChild VIEWQEQUIP = Widgets.getWidget(387,17);
    public static final WidgetChild EQUIPSCREEN = Widgets.getWidget(84,1);
    public static final WidgetChild EQUIPCLOSE = Widgets.getWidget(84,4);

    //POLL BOOTH
    public static final WidgetChild POLL = Widgets.getWidget(345,0);
    public static final WidgetChild POLLCLOSE = Widgets.getWidget(345,1);

    //MAGIC
    public static final WidgetChild WINDBOLT = Widgets.getWidget(218,2);

    //SETTINGS
    public static final WidgetChild ADVANCED = Widgets.getWidget(261,11);
    public static final WidgetChild ROOF = Widgets.getWidget(60,8);
    public static final WidgetChild ADVANCEDCLOSE = Widgets.getWidget(60,2).getChild(11);

    // final int PARENT = 269;
    // int [] childValues = {113, 114, 115, 116, 117, 118, 119, 121, 127, 129, 130, 131};
}
