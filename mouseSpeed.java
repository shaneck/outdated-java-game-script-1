package tutorialIsland;

import org.tbot.methods.Mouse;
import org.tbot.methods.Random;

/**
 * Created by Shaneee on 19/06/2015.
 */
public class mouseSpeed extends Thread {

    private Thread t;
    private String threadName;

    mainHandler mainhandler = new mainHandler();

    mouseSpeed(String name){
        threadName = name;
        mainhandler.log("Creating Thread: " + threadName );
    }
    public int Mousespeed(){

        Mouse.setSpeed(Random.nextInt(30, 35));
        return Random.nextInt(1000, 1500);
    }

    public void run(){
        mainhandler.log("Starting randomness");
        Mousespeed();
    }

    public void start(){
        mainhandler.log("Starting " + threadName);

        if (t == null)
        {
            t = new Thread(this, threadName);
            t.start();
        }
}



}
