package tutorialIsland;
import org.tbot.bot.Account;
import org.tbot.bot.TBot;
import org.tbot.internal.event.listeners.PaintListener;
import org.tbot.methods.tabs.Emotes;
import org.tbot.methods.tabs.Equipment;
import org.tbot.util.Filter;
import org.tbot.wrappers.*;
import org.tbot.internal.AbstractScript;
import org.tbot.internal.Manifest;
import org.tbot.internal.handlers.LogHandler;
import org.tbot.internal.handlers.RandomHandler;
import org.tbot.methods.*;
import org.tbot.methods.Widgets;
import org.tbot.methods.tabs.Inventory;
import org.tbot.methods.walking.Path;
import org.tbot.methods.walking.Walking;
import org.tbot.util.Condition;
import java.awt.*;
import org.tbot.methods.input.keyboard.Keyboard;

import java.awt.event.KeyEvent;

import static org.tbot.methods.Time.sleep;

@Manifest(name = "Tutorial Island", authors = "Crithane", version = 1.0)
public class mainHandler extends AbstractScript implements PaintListener {
    private WidgetChild appearanceAccept() {
        return Widgets.getWidget(269, 100);
    }
    public String currentState;
    private Player me = Players.getLocal();
    public static String email1 = "";
    public static String password2 = "";
    public int roof = 0;
    public int accountsComplete = 0;
    private boolean firstTime = true;
    private boolean firstTime2 = true;
    private long currentTime, startTime;
    public static boolean taken = false;
    public static boolean accountDone = false;

    private Timer t = new Timer();

    @Override
    public boolean onStart() {
        if(firstTime2 == true){
            startTime = System.currentTimeMillis();
            firstTime2 = false;
        }
        TBot.getBot().getScriptHandler().getRandomHandler().disableAll();
        TBot.getBot().getScriptHandler().getRandomHandler().get(RandomHandler.BOT_WORLD).enable();
        if(!Game.isLoggedIn() && firstTime == true) {
            currentState = "Creating Account";
            GUI.run();
            firstTime = false;
        }else if(!Game.isLoggedIn()){
            try {
                AccountCreation.AccountDetails(AccountCreation.emailString, AccountCreation.domainString, AccountCreation.passwordString);
                AccountLog();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        sleep(2000, 3000);
        return true;
    }

    public static boolean LoginHandler(String email, String password)  {
        email1 = email;
        password2 = password;
        TBot.getBot().setCurrentAccount(new Account(email, password, "", Skills.Skill.Cooking));
        TBot.getBot().getScriptHandler().getRandomHandler().get(RandomHandler.AUTO_LOGIN).enable();
        return true;
    }
    public int loop() {
        if(Variables.CONTINUE4 != null|| Variables.CONTINUE5 != null) {
            if (Variables.CONTINUE4.isOnScreen() || Variables.CONTINUE5.isOnScreen()) {
                handleContinue2();
            }
        }
        Mouse.setSpeed(Random.nextInt(30, 35));
        while (taken == true) {
            try{
                AccountCreation.AccountDetails(GUI.emailString, GUI.domainString, GUI.passwordString);
                AccountLog();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (Game.isLoggedIn()) {
            final int SETTING = Settings.get(281);
            if (me.getAnimation() == -1) {
                if (onTutorialIsland() == true || appearanceAccept().isOnScreen()) {
                    switch (SETTING) {
                        case 0:
                            roof = 0;
                            //randomise appearance
                            if (appearanceAccept().isOnScreen()) {
                                charCreation();

                            } else {
                                //Talk to get started guide
                                currentState = "Getting Started";
                                if (!Variables.CONTINUE.isOnScreen()) {
                                    methods.talkTo("RuneScape Guide");
                                    Time.sleepUntil(new Condition() {
                                                        @Override
                                                        public boolean check() {
                                                            return SETTING == 3;
                                                        }
                                                    },
                                            Random.nextInt(900, 1150)
                                    );
                                } else {
                                    handleContinue();
                                }
                            }

                            break;
                        case 3:
                            //Click tools
                            if (Variables.CONTINUE.isOnScreen() || Variables.CONTINUE2.isOnScreen()) {
                                handleContinue();
                            }
                            sleep(300, 600);
                            Variables.TOOLS.interact("Options");
                            Time.sleepUntil(new Condition() {
                                                @Override
                                                public boolean check() {
                                                    return Widgets.isOpen(Widgets.TAB_OPTIONS);
                                                }
                                            },
                                    Random.nextInt(900, 1150)
                            );
                            break;
                        case 7:
                            if(roof == 0){
                                if(Variables.ADVANCED != null && Variables.ADVANCED.isOnScreen() && !Variables.ROOF.isOnScreen()){
                                    Variables.ADVANCED.interact("View Advanced Options");
                                    sleep(600,800);
                                }else if (!Variables.ROOF.isOnScreen()){
                                    Variables.TOOLS.interact("Options");
                                    sleep(600,800);
                                }else if(Variables.ROOF.isOnScreen()){
                                    int textID = Variables.ROOF.getTextureID();
                                    if(textID == 761) {
                                        Variables.ROOF.interact("Roof-removal");
                                        sleep(600, 800);
                                        roof = 1;
                                        Widgets.getWidget(60, 2).getChild(11).interact("Close");
                                        sleep(600, 800);
                                    }else if(textID == 762){
                                        roof = 1;
                                        Widgets.getWidget(60, 2).getChild(11).interact("Close");
                                        sleep(600, 800);
                                    }else{
                                        roof = 1;
                                        Widgets.getWidget(60, 2).getChild(11).interact("Close");
                                        sleep(600, 800);
                                    }
                                }
                            }else {
                                if (!Variables.CONTINUE.isOnScreen()) {
                                    methods.talkTo("RuneScape Guide");
//                        guide.interact("Talk-to");
                                    Time.sleepUntil(new Condition() {
                                                        @Override
                                                        public boolean check() {
                                                            return Variables.CONTINUE.isOnScreen();
                                                        }
                                                    },
                                            Random.nextInt(900, 1150)
                                    );
                                } else {
                                    handleContinue();
                                }
                            }
                            break;
                        case 10:
                            currentState = "Making way to survival expert";
                            methods.interactObject("Door", "Open");
                            Time.sleepUntil(new Condition() {
                                                @Override
                                                public boolean check() {
                                                    return SETTING == 20;
                                                }
                                            }, // ooo using java 8 ha ;)
                                    Random.nextInt(3155, 4222)
                            );

                            //Walk through door
                            break;
                        case 20:
                            final Tile SurvivalTile = new Tile(3102, 3096, 0);
                            if (!Variables.CONTINUE.isOnScreen() && SurvivalTile.distance() > 5) {
                                Path survivalPath = Walking.findLocalPath(SurvivalTile);
                                survivalPath.traverse();
                                Time.sleepUntil(new Condition() {
                                                    @Override
                                                    public boolean check() {
                                                        return SurvivalTile.distance() < 5;
                                                    }
                                                },
                                        Random.nextInt(1000, 1300)
                                );
                            } else if (!Variables.CONTINUE.isOnScreen() && SurvivalTile.distance() < 5) {
                                methods.talkTo("Survival Expert");
                                Time.sleepUntil(new Condition() {
                                                    @Override
                                                    public boolean check() {
                                                        return Variables.CONTINUE.isOnScreen();
                                                    }
                                                },
                                        Random.nextInt(1000, 1300)
                                );
                            } else {
                                handleContinue();
                            }
                            break;
                        case 30:
                            if (Variables.CONTINUE.isOnScreen() || Variables.CONTINUE2.isOnScreen()) {
                                handleContinue();
                            }
                            Variables.INVENTORY.interact("Inventory");
                            Time.sleepUntil(new Condition() {
                                                @Override
                                                public boolean check() {
                                                    return Widgets.isOpen(Widgets.TAB_INVENTORY);
                                                }
                                            },
                                    Random.nextInt(900, 1150)
                            );
                            //Open inventory tag
                            break;
                        case 40:
                            currentState = "Getting wood for fire";
                            if (methods.doingNothing()) {
                                methods.interactObject("Tree", "Chop down");
                            }
                            Time.sleepUntil(new Condition() {
                                                @Override
                                                public boolean check() {
                                                    return Inventory.containsOneOf(2511);
                                                }
                                            },
                                    Random.nextInt(1000, 1300)
                            );
                            //cut down tree
                            break;
                        case 50:
                            currentState = "Lighting fire";
                            final GameObject fire = GameObjects.getNearest(26185);
                            if (fire != null && fire.getLocation().distance() < 1) {
                                GameObject null1 = GameObjects.getNearest(320);
                                Path pathToNull = Walking.findLocalPath(null1.getLocation());
                                pathToNull.traverse();
                                Time.sleepUntil(new Condition() {
                                                    @Override
                                                    public boolean check() {
                                                        return fire.getLocation().distance() > 1;
                                                    }
                                                },
                                        Random.nextInt(1000, 1300)
                                );
                            } else {
                                Item log = Inventory.getItemClosestToMouse(2511);
                                Item TinderBox = Inventory.getItemClosestToMouse(590);
                                if (log != null && TinderBox != null) {
                                    Inventory.useItemOn(log, TinderBox);
                                }
                                Time.sleepUntil(new Condition() {
                                                    @Override
                                                    public boolean check() {
                                                        return !Inventory.contains(2511);
                                                    }
                                                },
                                        Random.nextInt(1000, 1300)
                                );
                            }
                            break;
                        case 60:
                            Variables.LEVELS.interact("Stats");
                            Time.sleepUntil(new Condition() {
                                                @Override
                                                public boolean check() {
                                                    return Widgets.isOpen(Widgets.TAB_STATS);
                                                }
                                            },
                                    Random.nextInt(900, 1150)
                            );
                            //Open skills tab
                            break;
                        case 70:
                            if (Variables.CONTINUE.isOnScreen() || Variables.CONTINUE2.isOnScreen()) {
                                handleContinue();
                            } else {
                                methods.talkTo("Survival Expert");
                                Time.sleepUntil(new Condition() {
                                                    @Override
                                                    public boolean check() {
                                                        return Variables.CONTINUE.isOnScreen() || Variables.CONTINUE2.isOnScreen();
                                                    }
                                                },
                                        Random.nextInt(1000, 1300)
                                );
                            }
                            //Talk to survival expert
                            break;
                        case 80:
                            if(Npcs.getNearest("Fishing spot").distance() < 5 && Npcs.getNearest("Fishing spot").isOnScreen() && Npcs.getNearest("Fishing spot") != null) {
                                currentState = "Fishing";
                                if (methods.doingNothing()) {
                                    if(Inventory.hasItemSelected()){
                                        Inventory.deselectItem();
                                    }
                                    methods.interact("Fishing spot", "Net");
                                }
                                Time.sleepUntil(new Condition() {
                                                    @Override
                                                    public boolean check() {
                                                        return SETTING == 90;
                                                    }
                                                },
                                        Random.nextInt(1000, 1300)
                                );
                                sleep(200, 300);
                            }else{
                                if (Npcs.getNearest("Fishing spot") != null) {
                                    Walking.walkTileMM(Npcs.getNearest("Fishing spot").getLocation());
                                    sleep(600,800);
                                }
                            }
                            //Fish shrimp
                            break;
                        case 90:
                            currentState = "Cooking";

                            GameObject fires = GameObjects.getNearest(26185);
                            if (fires != null) {
                                if (Widgets.isOpen(Widgets.TAB_INVENTORY)) {
                                    if (methods.doingNothing()) {
                                        Inventory.useItemOn(2514, fires);
                                        Time.sleepUntil(new Condition() {
                                                            @Override
                                                            public boolean check() {
                                                                return SETTING == 110;
                                                            }
                                                        },
                                                Random.nextInt(1000, 1300)
                                        );
                                    }
                                } else {
                                    Variables.INVENTORY.interact("Inventory");
                                    Time.sleepUntil(new Condition() {
                                                        @Override
                                                        public boolean check() {
                                                            return Widgets.isOpen(Widgets.TAB_INVENTORY);
                                                        }
                                                    },
                                            Random.nextInt(1000, 1300)
                                    );
                                }
                            } else {
                                GameObject tree3 = GameObjects.getNearest(9730);
                                tree3.interact("Chop");
                                Time.sleepUntil(new Condition() {
                                                    @Override
                                                    public boolean check() {
                                                        return Inventory.containsOneOf(2511);
                                                    }
                                                },
                                        Random.nextInt(1000, 1300)
                                );
                                if (!Widgets.isOpen(Widgets.TAB_INVENTORY)) {
                                    Variables.INVENTORY.interact("Inventory");
                                }
                                Time.sleepUntil(new Condition() {
                                                    @Override
                                                    public boolean check() {
                                                        return Widgets.isOpen(Widgets.TAB_INVENTORY);
                                                    }
                                                },
                                        Random.nextInt(1000, 1300)
                                );
                                Inventory.useItemOn(2511, 590);
                                Time.sleepUntil(new Condition() {
                                                    @Override
                                                    public boolean check() {
                                                        return !Inventory.contains(2511);
                                                    }
                                                },
                                        Random.nextInt(1000, 1300)
                                );
                            }

                            //Cook shrimp
                            break;
                        case 110:
                            currentState = "Re-Cooking";
                            if (!Inventory.contains("Raw shrimps")) {
                                if (!Inventory.contains("Raw shrimps")) {
                                    if (Npcs.getNearest("Fishing spot").distance() < 5 && Npcs.getNearest("Fishing spot").isOnScreen() && Npcs.getNearest("Fishing spot") != null) {
                                        currentState = "Fishing";
                                        if (methods.doingNothing()) {
                                            if(Inventory.hasItemSelected()){
                                                Inventory.deselectItem();
                                            }
                                            methods.interact("Fishing spot", "Net");
                                        }
                                        Time.sleepUntil(new Condition() {
                                                            @Override
                                                            public boolean check() {
                                                                return SETTING == 90;
                                                            }
                                                        },
                                                Random.nextInt(1000, 1300)
                                        );
                                        sleep(200, 300);
                                    } else {
                                        if (Npcs.getNearest("Fishing spot") != null) {
                                            Walking.walkTileMM(Npcs.getNearest("Fishing spot").getLocation());
                                            sleep(600, 800);
                                        }
                                        }
                                    }
                                }
                            GameObject fires2 = GameObjects.getNearest(26185);
                            if (fires2 != null) {
                                if (Widgets.isOpen(Widgets.TAB_INVENTORY)) {
                                    if (methods.doingNothing()) {
                                        Inventory.useItemOn(2514, fires2);
                                        Time.sleepUntil(new Condition() {
                                                            @Override
                                                            public boolean check() {
                                                                return SETTING == 120;
                                                            }
                                                        },
                                                Random.nextInt(1000, 1300)
                                        );
                                    }
                                } else {
                                    Variables.INVENTORY.interact("Inventory");
                                    Time.sleepUntil(new Condition() {
                                                        @Override
                                                        public boolean check() {
                                                            return Widgets.isOpen(Widgets.TAB_INVENTORY);
                                                        }
                                                    },
                                            Random.nextInt(1000, 1300)
                                    );
                                }
                            }
                            //Burnt shrimp, catch one more light fire and cook
                            break;
                        case 120:
                            currentState = "Walking to Chef";
                            final Tile gate = new Tile(3091, 3092, 0);
                            if (gate.distance() > 5) {
                                Path gatePath = Walking.findLocalPath(gate);
                                gatePath.traverse();
                                Time.sleepUntil(new Condition() {
                                                    @Override
                                                    public boolean check() {
                                                        return gate.distance() < 5;
                                                    }
                                                },
                                        Random.nextInt(1000, 1300)
                                );
                            } else {
                                methods.interactObject("Gate", "Open");
                                Time.sleepUntil(new Condition() {
                                                    @Override
                                                    public boolean check() {
                                                        return SETTING == 130;
                                                    }
                                                },
                                        Random.nextInt(1000, 1300)
                                );
                            }
                            //Walk to gate and open
                            break;
                        case 130:
                            sleep(1000, 1200);
                            currentState = "Walking to Chef";
                            final Tile cook = new Tile(3079, 3084, 0);
                            if (cook.distance() > 5) {
                                if (Walking.findLocalPath(cook) != null) {
                                    Walking.findLocalPath(cook).traverse();
                                    Time.sleepUntil(new Condition() {
                                                        @Override
                                                        public boolean check() {
                                                            return cook.distance() < 5;
                                                        }
                                                    },
                                            Random.nextInt(1000, 1300)
                                    );
                                }
                            } else {
                                methods.interactObject("Door", "Open");
                                Time.sleepUntil(new Condition() {
                                                    @Override
                                                    public boolean check() {
                                                        return SETTING == 140;
                                                    }
                                                },
                                        Random.nextInt(1000, 1300)
                                );
                            }
                            //Walk to cook door and open
                            break;
                        case 140:
                            currentState = "Talking to Chef";
                            if (!Variables.CONTINUE.isOnScreen() && !Variables.CONTINUE2.isOnScreen() && !Variables.CONTINUE3.isOnScreen()) {
                                methods.talkTo("Master Chef");
                                Time.sleepUntil(new Condition() {
                                                    @Override
                                                    public boolean check() {
                                                        return Variables.CONTINUE.isOnScreen() || Variables.CONTINUE2.isOnScreen() || Variables.CONTINUE3.isOnScreen();
                                                    }
                                                },
                                        Random.nextInt(1000, 1300)
                                );
                            } else if (Variables.CONTINUE.isOnScreen() || Variables.CONTINUE2.isOnScreen() || Variables.CONTINUE3.isOnScreen()) {
                                handleContinue();
                                Time.sleepUntil(new Condition() {
                                                    @Override
                                                    public boolean check() {
                                                        return SETTING == 150;
                                                    }
                                                },
                                        Random.nextInt(1000, 1300)
                                );
                            }
                            //Talk to cook
                            break;
                        case 150:
                            currentState = "Making Bread";
                            Random rand3 = new Random();
                            if (rand3.nextInt(2) == 1) {
                                Inventory.useItemOn(2516, 1929);
                                Time.sleepUntil(new Condition() {
                                                    @Override
                                                    public boolean check() {
                                                        return SETTING == 160;
                                                    }
                                                },
                                        Random.nextInt(1000, 1400)
                                );
                            } else {
                                Inventory.useItemOn(1929, 2516);
                                Time.sleepUntil(new Condition() {
                                                    @Override
                                                    public boolean check() {
                                                        return SETTING == 160;
                                                    }
                                                },
                                        Random.nextInt(1000, 2000)
                                );
                            }
                            //Combine flour and bucket of water
                            break;
                        case 160:
                            currentState = "Making bread";
                            if (me.getAnimation() == -1) {
                                if (Inventory.useItemOn(2307, GameObjects.getNearest("Range"))) {
                                    Time.sleepUntil(new Condition() {
                                                        @Override
                                                        public boolean check() {
                                                            return SETTING == 170;
                                                        }
                                                    },
                                            Random.nextInt(1000, 2000)
                                    );
                                }
                            }
                            //Use bread and dough with range
                            break;
                        case 170:
                            Variables.MUSIC.interact("Music Player");
                            Time.sleepUntil(new Condition() {
                                                @Override
                                                public boolean check() {
                                                    return Widgets.isOpen(Widgets.TAB_MUSIC);
                                                }
                                            },
                                    Random.nextInt(900, 1150)
                            );
                            //Open music tab
                            break;
                        case 180:
                            currentState = "Leaving cook";
                            final Tile leaveCook = new Tile(3071, 3090, 0);
                            if(leaveCook != null) {
                                Path leaveCookPath = Walking.findLocalPath(leaveCook);
                                if (leaveCookPath !=null) {
                                    leaveCookPath.traverse();
                                    Time.sleepUntil(new Condition() {
                                                        @Override
                                                        public boolean check() {
                                                            return leaveCook.distance() < 2;
                                                        }
                                                    },
                                            Random.nextInt(1000, 1500)
                                    );
                                }
                            }
                            //leave cooks building
                            break;
                        case 183:
                            currentState = "Random Emote";
                            Variables.EMOTES.interact("Emotes");
                            Time.sleepUntil(new Condition() {
                                                @Override
                                                public boolean check() {
                                                    return Widgets.isOpen(Widgets.TAB_EMOTES);
                                                }
                                            },
                                    Random.nextInt(900, 1150)
                            );
                            //open emote tab
                            break;
                        case 187:

                            switch (Random.nextInt(1, 20)) {
                                case 1:
                                    Emotes.doEmote(Emotes.Emote.YES);
                                    break;
                                case 2:
                                    Emotes.doEmote(Emotes.Emote.NO);
                                    break;
                                case 3:
                                    Emotes.doEmote(Emotes.Emote.BOW);
                                    break;
                                case 4:
                                    Emotes.doEmote(Emotes.Emote.ANGRY);
                                    break;
                                case 5:
                                    Emotes.doEmote(Emotes.Emote.THINK);
                                    break;
                                case 6:
                                    Emotes.doEmote(Emotes.Emote.WAVE);
                                    break;
                                case 7:
                                    Emotes.doEmote(Emotes.Emote.SHRUG);
                                    break;
                                case 8:
                                    Emotes.doEmote(Emotes.Emote.CHEER);
                                    break;
                                case 9:
                                    Emotes.doEmote(Emotes.Emote.BECKON);
                                    break;
                                case 10:
                                    Emotes.doEmote(Emotes.Emote.JUMP_FOR_JOY);
                                    break;
                                case 11:
                                    Emotes.doEmote(Emotes.Emote.DANCE);
                                    break;
                                case 12:
                                    Emotes.doEmote(Emotes.Emote.JIG);
                                    break;
                                case 13:
                                    Emotes.doEmote(Emotes.Emote.SPIN);
                                    break;
                                case 14:
                                    Emotes.doEmote(Emotes.Emote.HEADBANG);
                                    break;
                                case 15:
                                    Emotes.doEmote(Emotes.Emote.BLOW_KISS);
                                    break;
                                case 16:
                                    Emotes.doEmote(Emotes.Emote.PANIC);
                                    break;
                                case 17:
                                    Emotes.doEmote(Emotes.Emote.RASPBERRY);
                                    break;
                                case 18:
                                    Emotes.doEmote(Emotes.Emote.LAUGH);
                                    break;
                                case 19:
                                    Emotes.doEmote(Emotes.Emote.YAWN);
                                    break;
                                case 20:
                                    Emotes.doEmote(Emotes.Emote.CRY);
                                    break;
                            }
                            //do random emote
                            break;
                        case 190:
                            Variables.TOOLS.interact("Options");
                            sleep(600, 800);
                            break;
                        case 200:
                            Variables.RUNTAB.interact("Toggle Run");
                            sleep(1000, 2000);
                            //enable run
                            break;
                        case 210:
                            currentState = "Making way to quest guide";
                            final Tile quest = new Tile(3086, 3127, 0);
                            if (quest.distance() > 4) {
                                Path questPath = Walking.findLocalPath(quest);
                                questPath.traverse();
                                Time.sleepUntil(new Condition() {
                                                    @Override
                                                    public boolean check() {
                                                        return quest.distance() < 4;
                                                    }
                                                },
                                        Random.nextInt(1000, 2000)
                                );
                            } else if (quest.distance() < 4) {
                                methods.interactObject("Door", "Open");
                                Time.sleepUntil(new Condition() {
                                                    @Override
                                                    public boolean check() {
                                                        return SETTING == 220;
                                                    }
                                                },
                                        Random.nextInt(1000, 1200)
                                );
                            }
                            //run to quest door and open
                            break;
                        case 220:currentState = "Talking to quest guide";

                            methods.talkTo("Quest Guide");
                            Time.sleepUntil(new Condition() {
                                                @Override
                                                public boolean check() {
                                                    return SETTING == 240;
                                                }
                                            },
                                    Random.nextInt(1000, 1200)
                            );
                            //talk to quest guide
                            break;
                        case 230:
                            if (!Widgets.isOpen(Widgets.TAB_QUESTS)) {
                                Widgets.openTab(Widgets.TAB_QUESTS);
                                sleep(1000, 1200);
                            }
                        case 240:
                            if (!Variables.CONTINUE.isOnScreen() && !Variables.CONTINUE2.isOnScreen() && !Variables.CONTINUE3.isOnScreen()) {
                                if (!Widgets.isOpen(Widgets.TAB_QUESTS)) {
                                    Widgets.openTab(Widgets.TAB_QUESTS);
                                }
                                Time.sleepUntil(new Condition() {
                                                    @Override
                                                    public boolean check() {
                                                        return Widgets.isOpen(Widgets.TAB_QUESTS);
                                                    }
                                                },
                                        Random.nextInt(1000, 1200)
                                );
                                methods.talkTo("Quest Guide");
                                Time.sleepUntil(new Condition() {
                                                    @Override
                                                    public boolean check() {
                                                        return SETTING == 250;
                                                    }
                                                },
                                        Random.nextInt(3000, 4000)
                                );
                            } else {
                                handleContinue();
                            }
                            //open quest tab
                            break;
                        case 250:
                            currentState = "Going to mining guide";
                            final Tile ladder = new Tile(3087, 3120, 0);
                            Path ladderPath = Walking.findLocalPath(ladder);
                            ladderPath.traverse();
                            sleep(1000, 1200);
                            if (methods.interactMobject("Ladder", "Climb-down")) {
                                Time.sleepUntil(new Condition() {
                                                    @Override
                                                    public boolean check() {
                                                        return SETTING == 260;
                                                    }
                                                },
                                        Random.nextInt(1000, 1200)
                                );
                            }
                            //go down  ladder
                            break;
                        case 260:
                            final Tile mining = new Tile(3081, 9505, 0);
                            if (mining.distance() > 5) {
                                Path miningPath = Walking.findLocalPath(mining);
                                miningPath.traverse();
                                Time.sleepUntil(new Condition() {
                                                    @Override
                                                    public boolean check() {
                                                        return mining.distance() < 5;
                                                    }
                                                },
                                        Random.nextInt(2000, 4000)
                                );
                            }
                            methods.talkTo("Mining Instructor");
                            //talk to mining dude
                            break;
                        case 270:
                            currentState = "Prospecting";
                            if (GameObjects.getNearest(10080).isOnScreen()) {
                                methods.interactObjectID(10080, "Prospect");
                                Time.sleepUntil(new Condition() {
                                                    @Override
                                                    public boolean check() {
                                                        return SETTING == 280;
                                                    }
                                                },
                                        Random.nextInt(3000, 4000)
                                );
                            } else {
                                Tile tin = new Tile(3077, 9506, 0);
                                Walking.findLocalPath(tin).traverse();
                                sleep(400, 600);
                            }
                            //prospect tin ore
                            break;
                        case 280:
                            if (GameObjects.getNearest(10079).isOnScreen()) {
                                if(GameObjects.getNearest(10079) !=null){
                                    methods.interactObjectID(10079, "Prospect");
                                    Time.sleepUntil(new Condition() {
                                                        @Override
                                                        public boolean check() {
                                                            return SETTING == 290;
                                                        }
                                                    },
                                            Random.nextInt(3000, 4000)
                                    );
                                }
                            } else {
                                // 3085, 9501, 0
                                Tile copper = new Tile(3084, 9503, 0);
                                Walking.findLocalPath(copper).traverse();
                                sleep(400, 600);
                            }
                            //prospect copper ore
                            break;
                        case 290:
                            methods.talkTo("Mining Instructor");
                            //talk to mining dude
                            break;
                        case 300:
                            currentState = "Mining tin and copper";
                            Tile tin = new Tile(3078, 9504, 0);
                            if (GameObjects.getNearest(10080).isOnScreen() && GameObjects.getNearest(10080) !=null && Walking.findLocalPath(tin) != null) {
                                methods.interactObjectID(10080, "Mine");
                                Time.sleepUntil(new Condition() {
                                                    @Override
                                                    public boolean check() {
                                                        return SETTING == 310;
                                                    }
                                                },
                                        Random.nextInt(3000, 4000)
                                );
                            }else{
                                if (Walking.findLocalPath(tin) !=null) {
                                    if(tin !=null) {
                                        Walking.findLocalPath(tin).traverse();
                                        sleep(400, 600);
                                    }
                                }
                            }
                            //mine tin ore
                            break;
                        case 310:
                            Tile copper = new Tile(3084, 9503, 0);
                            if (GameObjects.getNearest(10079).isOnScreen() && GameObjects.getNearest(10079) !=null && Walking.findLocalPath(copper) != null) {
                                methods.interactObjectID(10079, "Mine");
                                Time.sleepUntil(new Condition() {
                                                    @Override
                                                    public boolean check() {
                                                        return SETTING == 320;
                                                    }
                                                },
                                        Random.nextInt(3000, 4000)
                                );
                            } else {
                                Walking.findLocalPath(copper).traverse();
                                sleep(400, 600);
                            }
                            //mine copper ore
                            break;
                        case 320:
                            currentState = "Making bronze bar";
                            if (!Widgets.isOpen(Widgets.TAB_INVENTORY)) {
                                Widgets.openTab(Widgets.TAB_INVENTORY);
                                Time.sleepUntil(new Condition() {
                                                    @Override
                                                    public boolean check() {
                                                        return SETTING == 320;
                                                    }
                                                },
                                        Random.nextInt(800, 1200)
                                );
                            } else {
                                Random rand4 = new Random();
                                if (rand4.nextInt(2) == 1) {
                                    GameObject furnace = GameObjects.getNearest(10082);
                                    Inventory.useItemOn(438, furnace);
                                    sleep(3000, 4000);
                                } else {
                                    GameObject furnace = GameObjects.getNearest(10082);
                                    Inventory.useItemOn(436, furnace);
                                    sleep(3000, 4000);
                                }
                            }
                            handleContinue();
                            //make bronze bar in furnace
                            break;
                        case 330:
                            if (Variables.CONTINUE4.isOnScreen() || Variables.CONTINUE5.isOnScreen() || Variables.CONTINUE6.isOnScreen()) {
                                handleContinue2();
                            } else {
                                final Tile mining2 = new Tile(3081, 9505, 0);
                                if (mining2.distance() > 5) {
                                    Path miningPath = Walking.findLocalPath(mining2);
                                    miningPath.traverse();
                                    Time.sleepUntil(new Condition() {
                                                        @Override
                                                        public boolean check() {
                                                            return mining2.distance() < 5;
                                                        }
                                                    },
                                            Random.nextInt(3000, 4000)
                                    );
                                }
                                methods.talkTo("Mining Instructor");
                            }
                            //talk to mining dude
                            break;
                        case 340:
                            currentState = "Making bronze dagger";
                            if (Variables.CONTINUE.isOnScreen() || Variables.CONTINUE2.isOnScreen() || Variables.CONTINUE3.isOnScreen()) {
                                handleContinue();
                            } else if (Variables.CONTINUE4.isOnScreen() || Variables.CONTINUE5.isOnScreen()) {
                                handleContinue2();
                            } else {
                                if (Inventory.isOpen()) {
                                    if (methods.doingNothing()) {
                                        if (GameObjects.getNearest("Anvil").distance() < 5 && GameObjects.getNearest("Anvil") != null) {
                                            methods.useItemOn("Bronze bar", "Anvil");
                                        } else {
                                            Tile anvil = new Tile(3079, 9499, 0);
                                            Walking.findLocalPath(anvil).traverse();
                                            sleep(400, 600);
                                        }
                                    }
                                } else {
                                    Inventory.openTab();
                                    sleep(400, 500);
                                }
                                sleep(800, 900);
                            }
                            //use bar on anvil
                            break;
                        case 350:
                            if (Variables.CONTINUE.isOnScreen() || Variables.CONTINUE2.isOnScreen() || Variables.CONTINUE3.isOnScreen() || Variables.CONTINUE6.isOnScreen()) {
                                handleContinue();
                            } else {
                                if (Variables.CONTINUE4.isOnScreen() || Variables.CONTINUE5.isOnScreen()) {
                                    handleContinue2();
                                } else {
                                    if (Variables.SMITHING.isOnScreen()) {
                                        if (Inventory.contains("Bronze bar")) {
                                            if (methods.doingNothing()) {
                                                Variables.SMITHING.interact("Smith 1");
                                                sleep(200, 400);
                                            }
                                        }
                                    } else if (!Variables.SMITHING.isOnScreen()) {
                                        if (GameObjects.getNearest("Anvil").isOnScreen() && GameObjects.getNearest("Anvil") != null) {
                                            if(methods.doingNothing()) {
                                                GameObjects.getNearest("Anvil").interact("Smith");
                                                Time.sleepUntil(new Condition() {
                                                                    @Override
                                                                    public boolean check() {
                                                                        return Variables.SMITHING.isOnScreen();
                                                                    }
                                                                },
                                                        Random.nextInt(1000, 1300)
                                                );
                                            }

                                        } else if (!GameObjects.getNearest("Anvil").isOnScreen() && GameObjects.getNearest("Anvil") != null) {
                                            if (GameObjects.getNearest("Anvil").distance() > 5) {
                                                Tile anvil = new Tile(3077, 9399, 0);
                                                Walking.findLocalPath(anvil).traverse();
                                                Time.sleepUntil(new Condition() {
                                                                    @Override
                                                                    public boolean check() {
                                                                        return GameObjects.getNearest("Anvil").distance() < 5;
                                                                    }
                                                                },
                                                        Random.nextInt(1000, 1300)
                                                );

                                            }

                                        } else if (!GameObjects.getNearest("Anvil").isOnScreen() && GameObjects.getNearest("Anvil") == null) {
                                            Tile anvil = new Tile(3077, 9399, 0);
                                            Walking.findLocalPath(anvil).traverse();
                                            Time.sleepUntil(new Condition() {
                                                                @Override
                                                                public boolean check() {
                                                                    return GameObjects.getNearest("Anvil").distance() < 5;
                                                                }
                                                            },
                                                    Random.nextInt(1000, 1300)
                                            );
                                        }
                                    }
                                }
                            }
                            //smith bronze dagger
                            break;
                        case 360:
                            currentState = "Going to combat guide";
                            if (GameObjects.getNearest("Gate").distance() > 5 && GameObjects.getNearest("Gate") !=null) {
                                if( Walking.findLocalPath(GameObjects.getNearest("Gate")) != null) {
                                    Walking.findLocalPath(GameObjects.getNearest("Gate")).traverse();
                                    sleep(400, 600);
                                }
                            } else if (GameObjects.getNearest("Gate") == null) {
                                sleep(1000, 2000);

                            } else {
                                if(GameObjects.getNearest("Gate") !=null && GameObjects.getNearest("Gate").isOnScreen()) {
                                    methods.interactObject("Gate", "Open");
                                    sleep(600, 800);
                                }
                            }


                            //walk to gate and open before combat instructor
                            break;
                        case 370:
                            if (!Npcs.getNearest("Combat Instructor").isOnScreen()) {
                                if(Walking.findLocalPath(Npcs.getNearest("Combat Instructor"))!= null)
                                    Walking.findLocalPath(Npcs.getNearest("Combat Instructor")).traverse();
                                sleep(300, 400);
                            } else if (Npcs.getNearest("Combat Instructor").isOnScreen()) {
                                methods.talkTo("Combat Instructor");
                                sleep(400, 600);
                            }
                            //talk to combat instructor
                            break;
                        case 390:
                            Equipment.openTab();
                            sleep(400, 600);
                            //open equipment tab
                            break;
                        case 400:
                            currentState = "Equipping dagger";
                            if (Variables.VIEWQEQUIP.isOnScreen()) {
                                Variables.VIEWQEQUIP.interact("View equipment stats");
                                sleep(400, 600);
                            } else {
                                Equipment.openTab();
                                sleep(400, 600);
                            }

                            //view worn equipment
                            break;
                        case 405:
                            if (Variables.EQUIPSCREEN.isOnScreen()){
                                Item dagger = Inventory.getFirst("Bronze dagger");
                                if (dagger != null) {
                                    Mouse.click(dagger.getRandomPoint(), true);
                                    sleep(600,800);
                                }
                            } else if (Variables.VIEWQEQUIP.isOnScreen() && !Variables.EQUIPSCREEN.isOnScreen()) {
                                if(Variables.VIEWQEQUIP !=null) {
                                    Variables.VIEWQEQUIP.interact("View equipment stats");
                                    sleep(400, 600);
                                }
                            } else {
                                Equipment.openTab();
                                sleep(400, 600);
                            }
                            //Wield dagger with worn equipment open
                            break;
                        case 410:
                            if (Variables.EQUIPSCREEN.isOnScreen()) {
                                Variables.EQUIPCLOSE.interact("Close");
                                sleep(400, 600);
                            } else {
                                methods.talkTo("Combat Instructor");
                                sleep(400, 600);
                            }
                            //Close interface and talk to combat instructor
                            break;
                        case 420:
                            currentState = "Sword and shield";
                            if(!Inventory.isOpen()){
                                Inventory.openTab();
                                sleep(400, 600);
                            }
                            if (Variables.VIEWQEQUIP.isOnScreen() && (Equipment.getItemInSlot(Equipment.SLOTS_WEAPON).getName() == "Bronze Dagger")) {
                                Equipment.getItemInSlot(Equipment.SLOTS_WEAPON).interact("Remove");
                                sleep(400, 600);
                            } else if (!Variables.VIEWQEQUIP.isOnScreen() && (Equipment.getItemInSlot(Equipment.SLOTS_WEAPON).getName() == "Bronze Dagger")) {
                                Equipment.openTab();
                                sleep(400, 600);
                            } else if (!Variables.VIEWQEQUIP.isOnScreen() && !(Equipment.getItemInSlot(Equipment.SLOTS_WEAPON).getName() == "Bronze Dagger")) {
                                if (Inventory.isOpen()) {
                                    if(Inventory.getFirst("Bronze sword") != null) {
                                        Inventory.getFirst("Bronze sword").interact("Wield");
                                        sleep(1000, 1600);
                                    }
                                    if(Inventory.getFirst("Wooden shield") != null) {
                                        Inventory.getFirst("Wooden shield").interact("Wield");
                                        sleep(1000, 1600);
                                    }
                                } else {
                                    Inventory.openTab();
                                    sleep(400, 600);
                                }
                            }
                            //Open Equipment tab and unequip dagger, then wield sword and shield
                            break;
                        case 430:
                            if(Variables.CONTINUE4.isOnScreen() || Variables.CONTINUE5.isOnScreen()){
                                handleContinue2();
                            }
                            Variables.EQUIPMENT.interact("Combat Options");
                            sleep(400, 600);
                            //open combat options tab
                            break;
                        case 440:
                            currentState = "Walking to rats";
                            GameObject gate2 = GameObjects.getNearest(9720);
                            if (gate2 != null && gate2.isOnScreen()) {
                                methods.interactObjectID(9720, "Open");
                                sleep(400, 600);
                            } else {
                                if (gate2 != null) {
                                    Path gatepath = Walking.findLocalPath(gate2);
                                    if (gatepath != null) {
                                        gatepath.traverse();
                                    }
                                    sleep(600, 800);
                                }
                            }
                            //open gate to the rats
                            break;
                        case 450:
                            currentState = "Attacking rat";
                            sleep(1000, 1200);
                            if (me.getAnimation() == -1){
                                NPC rat = Npcs.getNearest(new Filter<NPC>(){
                                    @Override
                                    public boolean accept(NPC npc) {
                                        return npc != null && npc.getName().toLowerCase().equals("giant rat")
                                                && (npc.getInteractingEntity() == null || npc.isInteractingWithLocalPlayer());}});
                                if (rat != null) {
                                    if (methods.doingNothing() && (!Players.getLocal().isHealthBarVisible() && (rat != null && !rat.isInteractingWithLocalPlayer()))) {
                                        rat.interact("Attack");
                                        sleep(600, 800);
                                    }
//                                    sleep(400, 600);
                                    return Random.nextInt(600, 800);
                                }
                            } else {
                                sleep(400, 600);
                            }
                            if(Variables.CONTINUE4.isOnScreen() || Variables.CONTINUE5.isOnScreen()){
                                handleContinue2();
                            }
                            //interact with one rat
                            break;
                        case 460:
                            if(Variables.CONTINUE4.isOnScreen() || Variables.CONTINUE5.isOnScreen()){
                                handleContinue2();
                            }
                            if (methods.doingNothing()) {
                                NPC rat = Npcs.getNearest(new Filter<NPC>(){
                                    @Override
                                    public boolean accept(NPC npc) {
                                        return npc != null && npc.getName().toLowerCase().equals("giant rat")
                                                && (npc.getInteractingEntity() == null || npc.isInteractingWithLocalPlayer());}});
                                if (rat.getInteractingEntity() == null) {
                                    if (methods.doingNothing()&& (!Players.getLocal().isHealthBarVisible() && (rat != null && !rat.isInteractingWithLocalPlayer()))) {
                                        rat.interact("Attack");
                                        sleep(600, 800);
                                    }
//                                    sleep(400, 600);
                                    return Random.nextInt(600, 800);
                                }
                            } else {
                                sleep(400, 600);
                            }
                            //in combat with rat
                            break;
                        case 470:
                            NPC combat = Npcs.getNearest("Combat Instructor");
                            if (combat != null && combat.distance() < 5) {
                                methods.talkTo("Combat Instructor");
//                                sleep(400, 600);
                                return Random.nextInt(600, 800);
                            } else {
                                if (combat != null && combat.distance() > 5) {
                                    if(combat != null) {
                                        Path combatpath = Walking.findLocalPath(combat);
                                        if(combatpath != null) {
                                            combatpath.traverse();
                                            sleep(800, 900);
                                        }
                                    }
                                }
                            }
                            if(Variables.CONTINUE4.isOnScreen() || Variables.CONTINUE5.isOnScreen()){
                                handleContinue2();
                            }
                            //Rat is dead, open gate and talk to combat instructor
                            break;
                        case 480:
                            currentState = "Attacking rat with bow";
                            if (!Inventory.isOpen()) {
                                Inventory.openTab();
                                sleep(400, 600);
                            }
                            if (Variables.CONTINUE.isOnScreen() || Variables.CONTINUE2.isOnScreen() || Variables.CONTINUE3.isOnScreen() || Variables.CONTINUE4.isOnScreen()) {
                                handleContinue();
                            }
                            if (Inventory.contains(882) || Inventory.containsOneOf(841)) {
                                if (!Inventory.isOpen()) {
                                    Inventory.openTab();
//                                    sleep(400, 600);
                                    return Random.nextInt(600, 800);
                                }
                                if (Inventory.containsOneOf(841)) {
                                    Inventory.getFirst(841).interact("Wield");
//                                        sleep(400, 600);
                                    return Random.nextInt(600, 800);
                                }
                                if (Inventory.containsAll(882)) {
                                    Inventory.getFirst(882).interact("Wield");
//                                        sleep(400, 600);
                                    return Random.nextInt(600, 800);
                                }
                            } else {
                                NPC rat = Npcs.getNearest(new Filter<NPC>(){
                                    @Override
                                    public boolean accept(NPC npc) {
                                        return npc != null && npc.getName().toLowerCase().equals("giant rat")
                                                && (npc.getInteractingEntity() == null || npc.isInteractingWithLocalPlayer());}});
                                if (rat.getInteractingEntity() == null) {
                                    if (methods.doingNothing()) {
                                        rat.interact("Attack");
                                    }
//                                    sleep(400, 600);
                                    return Random.nextInt(600, 800);
                                }
                            }
                            if(Variables.CONTINUE4.isOnScreen() || Variables.CONTINUE5.isOnScreen()){
                                handleContinue2();
                            }
                            //equip bronze arrows and shortbow and attack rat outside of cage
                            break;
                        case 490:
                            if (Inventory.contains(882) || Inventory.containsOneOf(841)) {
                                if (!Inventory.isOpen()) {
                                    Inventory.openTab();
//                                    sleep(400, 600);
                                    return Random.nextInt(600, 800);
                                }
                                if (Inventory.containsOneOf(841)) {
                                    Inventory.getFirst(841).interact("Wield");
//                                    sleep(400, 600);
                                    return Random.nextInt(600, 800);
                                }
                                if (Inventory.containsAll(882)) {
                                    Inventory.getFirst(882).interact("Wield");
//                                    sleep(400, 600);
                                    return Random.nextInt(600, 800);
                                }
                            } else {
                                NPC rat = Npcs.getNearest(new Filter<NPC>(){
                                    @Override
                                    public boolean accept(NPC npc) {
                                        return npc != null && npc.getName().toLowerCase().equals("giant rat")
                                                && (npc.getInteractingEntity() == null || npc.isInteractingWithLocalPlayer());}});
                                if (rat.getInteractingEntity() == null) {
                                    if (methods.doingNothing()) {
                                        if(!rat.isOnScreen()){
                                            Camera.turnTo(rat);
                                        }
                                        rat.interact("Attack");
                                    }
//                                    sleep(400, 600);
                                    return Random.nextInt(600, 800);
                                }
                            }
                            if(Variables.CONTINUE4.isOnScreen() || Variables.CONTINUE5.isOnScreen()){
                                handleContinue2();
                            }
                            //in combat with rat
                            break;
                        case 500:
                            currentState = "Leaving combat area";
                            if (GameObjects.getNearest("Ladder").distance() > 5) {
                                Tile ladder1 = new Tile(3111, 9525, 0);
                                Walking.findLocalPath(ladder1).traverse();
//                                sleep(400, 600);
                                return Random.nextInt(600, 800);
                            } else {
                                if (methods.doingNothing()) {
                                    methods.interactObject("Ladder", "Climb-up");
//                                sleep(400,600);
                                    return Random.nextInt(600, 800);
                                }
                            }
                            if(Variables.CONTINUE4.isOnScreen() || Variables.CONTINUE5.isOnScreen()){
                                handleContinue2();
                            }
                            //Rat is dead from ranged, walk to stairs and go up climb up the ladder
                            break;
                        case 510:
                            currentState = "Doing banking tutorial";
                            sleep(1000, 1200);
                            if (Npcs.getNearest("Banker").distance() > 5) {
                                Tile bank = new Tile(3123, 3122, 0);
                                Walking.findLocalPath(bank).traverse();
//                                sleep(400,600);
                                return Random.nextInt(600, 800);
                            } else {
                                if (Variables.YESBANK.isOnScreen()) {
                                    Widgets.getWidgetByText("Yes").interact("Continue");
                                    sleep(600, 800);
                                }
                                if (!Variables.CONTINUE.isOnScreen() && !Variables.CONTINUE2.isOnScreen() && !Variables.CONTINUE3.isOnScreen()) {
                                    methods.interact("Banker", "Talk-to");
                                    sleep(400, 600);
                                }
                                if (Variables.CONTINUE.isOnScreen() || Variables.CONTINUE2.isOnScreen() || Variables.CONTINUE3.isOnScreen()) {
                                    Widgets.clickContinue();
//                                    sleep(1000,1600);
                                    return Random.nextInt(600, 800);
                                }

                            }
                            //use bank booth, select option yest to open bank
                            break;
                        case 520:
                            currentState = "Poll booth tutorial";
                            if (Bank.isOpen()) {
                                Bank.close();
                                sleep(600, 800);
                            }
                            if (!Variables.CONTINUE.isOnScreen() && !Variables.CONTINUE2.isOnScreen() && !Variables.CONTINUE3.isOnScreen() && !Variables.CONTINUE6.isOnScreen() && !Variables.CONTINUE7.isOnScreen()) {
                                GameObject poll1 = GameObjects.getNearest("Poll booth");
                                if (poll1 != null && poll1.isOnScreen()) {
                                    methods.interactObject("Poll booth", "Use");
                                    return Random.nextInt(600, 800);
                                } else {
                                    Tile poll = new Tile(3120, 3121, 0);
                                    if(poll != null) {
                                        Path pollpath = Walking.findLocalPath(poll);
                                        if(pollpath !=null) {
                                            pollpath.traverse();
                                            sleep(600, 800);
                                        }
                                    }
                                }
                            } else if (Variables.CONTINUE.isOnScreen() || !Variables.CONTINUE2.isOnScreen() || Variables.CONTINUE3.isOnScreen() || Variables.CONTINUE6.isOnScreen() || Variables.CONTINUE7.isOnScreen()) {
                                handleContinue();
                                return Random.nextInt(600, 800);
                            }
                            //bank is open close bank and interact poll booth
                            break;
                        case 525:
                            currentState = "Moving to financial advisor";
                            if (Variables.POLL !=null &&Variables.POLL.isOnScreen()) {
                                WidgetChild close = Widgets.getWidget(345, 1).getChild(11);
                                if(close !=null) {
                                    close.interact("Close");
                                }
                                return Random.nextInt(600, 800);
                            }
                            GameObject nineseventwoone = GameObjects.getNearest(9721);
                            if (!Variables.POLL.isOnScreen() && nineseventwoone != null) {
                                if(nineseventwoone.distance() > 3) {
                                    Tile exitbank = new Tile(3124, 3124, 0);
                                    if (Walking.findLocalPath(exitbank) != null) {
                                        Walking.findLocalPath(exitbank).traverse();
                                        sleep(500, 600);
                                    }
                                }
                            } else if (!Variables.POLL.isOnScreen() && nineseventwoone != null) {
                                    methods.interactObjectID(9721, "Open");
                                    sleep(500, 600);
                            }
                            break;
                        //Poll booth interface is open, close it and open the door to financial advisor
                        case 530:
                            currentState = "Talking to FA";
                            sleep(1000, 1200);
                            if (Npcs.getNearest("Financial Advisor").isOnScreen()) {
                                methods.talkTo("Financial Advisor");
                                sleep(600, 800);
                            } else {
                                Walking.findLocalPath(Npcs.getNearest("Financial Advisor")).traverse();
                                sleep(600, 800);
                            }
                            //talk to financial advisor
                            break;
                        case 540:
                            currentState = "Leaving bank";
                            methods.interactObjectID(9722, "Open");
                            sleep(600, 800);
                            //continue through door
                            break;
                        case 550:
                            currentState = "Talking to Brother Brace";
                            sleep(1000, 1200);
                            Tile church = new Tile(3124, 3106, 0);
                            if (church.distance() > 5) {
                                Walking.findLocalPath(church).traverse();
                                sleep(600, 800);
                            } else {
                                methods.talkTo("Brother Brace");
                                sleep(600, 800);
                            }
                            //walk to and talk to brother brace
                            break;
                        case 560:
                            Variables.PRAYER.interact("Prayer");
                            sleep(600, 800);
                            //open prayer tab
                            break;
                        case 570:
                            methods.talkTo("Brother Brace");
                            sleep(600, 800);
                            //prayer is open, talk to brother brace
                            break;
                        case 580:
                            Variables.FRIENDS.interact("Friends List");
                            sleep(600, 800);
                            //open friends list
                            break;
                        case 590:
                            Variables.IGNORE.interact("Ignore List");
                            sleep(600, 800);
                            //open ignore list
                            break;
                        case 600:
                            methods.talkTo("Brother Brace");
                            sleep(600, 800);
                            //talk to brother brace
                            break;
                        case 610:
                            currentState = "Moving to magic Instructor";
                            if (GameObjects.getNearest("Door").isOnScreen()) {
                                GameObjects.getNearest("Door").interact("Open");
                                sleep(600, 800);
                            } else {
                                Walking.findLocalPath(GameObjects.getNearest("Door")).traverse();
                                sleep(600, 800);
                            }
                            //go through door to leave monastery
                            break;
                        case 620:
                            sleep(1000, 1200);
                            Tile wizard = new Tile(3141, 3086, 0);
                            if (wizard.distance() > 5) {
                                if(Walking.findLocalPath(wizard) != null) {
                                    Walking.findLocalPath(wizard).traverse();
                                    sleep(600, 800);
                                }
                            } else {
                                if (Npcs.getNearest("Magic Instructor").isOnScreen() && Npcs.getNearest("Magic Instructor") != null) {
                                    methods.talkTo("Magic Instructor");
                                    sleep(600, 800);
                                } else {
                                    if(Walking.findLocalPath(Npcs.getNearest("Magic Instructor"))!= null) {
                                        Walking.findLocalPath(Npcs.getNearest("Magic Instructor")).traverse();
                                        sleep(600, 800);
                                    }
                                }
                            }
                            //walk to and talk to magic instructor
                            break;
                        case 630:
                            currentState = "Opening magic tab";
                            Variables.MAGIC.interact("Magic");
                            sleep(600, 800);
                            //open magic tab
                            break;
                        case 640:
                            currentState = "Talking to magic instructor";
                            methods.talkTo("Magic Instructor");
                            sleep(600, 800);
                            //talk to magic instructor
                            break;
                        case 650:
                            currentState = "Casting on chicken";
                            Tile chicken = new Tile(3140, 3091, 0);
                            if (Npcs.getNearest("Chicken").distance() < 5 && Npcs.getNearest("Chicken").isOnScreen() && Npcs.getNearest("Chicken") !=null) {
                                if (Variables.WINDBOLT.isOnScreen()) {
                                    if(chicken.distance() < 3) {
                                        Variables.WINDBOLT.interact("Cast");
                                        sleep(600, 800);
                                        Npcs.getNearest("Chicken").interact("Cast");
                                        sleep(600, 800);
                                    }else{
                                        Walking.findLocalPath(chicken).traverse();
                                        sleep(1000, 1200);
                                    }
                                } else {
                                    Variables.MAGIC.interact("Magic");
                                    sleep(600, 800);
                                }
                            } else {
                                Walking.findLocalPath(chicken).traverse();
                                sleep(600, 800);
                            }
                            //cast windstrike

                            //cast wind strike on chicken
                            break;
                        case 670:
                            currentState = "heading to mainland!";
                            if(Variables.CONTINUE4.isOnScreen() || Variables.CONTINUE5.isOnScreen()){
                                handleContinue2();
                            }
                            if (Npcs.getNearest("Magic Instructor").isOnScreen()) {
                                if (!Variables.CONTINUE.isOnScreen() && !Variables.CONTINUE2.isOnScreen() && !Variables.CONTINUE3.isOnScreen() && !Variables.CONTINUE4.isOnScreen() && !Variables.CONTINUE7.isOnScreen() && !Variables.YESBANK.isOnScreen()) {
                                    Npcs.getNearest("Magic Instructor").interact("Talk-to");
                                    sleep(600, 800);
                                } else if (Variables.CONTINUE.isOnScreen() || Variables.CONTINUE2.isOnScreen() || Variables.CONTINUE3.isOnScreen() || Variables.CONTINUE4.isOnScreen() || Variables.CONTINUE7.isOnScreen()) {
                                    handleContinue();
                                    sleep(600, 800);
                                } else if (Variables.YESBANK.isOnScreen()) {
                                    Widgets.getWidgetByText("Yes").interact("Continue");
                                    sleep(600, 800);
                                }
                            } else {
                                Walking.findLocalPath(Npcs.getNearest("Magic Instructor")).traverse();
                                sleep(600, 800);
                            }
                            //almost done, talk to magic instructor, choose option yes to go to the main land
                            break;
                        case 1000:
                            currentState = "COMPLETE, restarting";
                            log("Tutorial Island COMPLETE!");
                            TBot.getBot().getScriptHandler().getRandomHandler().get(RandomHandler.AUTO_LOGIN).disable();
                            firstTime = false;
                            sleep(600, 800);
                            accountsComplete ++;
                            Game.logout();
                            sleep(2000, 3000);
                            onStart();
                            break;
                        default:
                            log("Case not accounted for in code, case: " + SETTING);
                            break;

                    }
                } else {
                }
            }
        }
        return Random.nextInt(240, 340);
    }

    public void log(String Text) {
        LogHandler.log(Text);
    }
    public static void handleContinue(){
        WidgetChild widget = Widgets.getWidgetByText("Click here to continue");
        if (widget != null && widget.isOnScreen()){
            Keyboard.pressKey((char) KeyEvent.VK_SPACE);
        }else{
            Keyboard.releaseKey((char) KeyEvent.VK_SPACE);
        }
    }
    public static void handleContinue2() {
        WidgetChild widget = Widgets.getWidgetByText("Click to continue");
        if (widget != null && widget.isOnScreen()) {
            Widgets.getWidgetByText("Click to continue");
            widget.click();
        }  // i need to buy a new mouse by the time we're done with this script l0l
    }
    private void charCreation() {
        Random rand = new Random();
        if (rand.nextInt(10) >= 5) {
            Variables.FEMALE_CHILD.interact("Female");
        }
        final int PARENT = 269;
        final int TOTALVALUES = 12;
        int[] changeValues = {113, 114, 115, 116, 117, 118, 119, 121, 127, 129, 130, 131};

        for (int i = 0; i < changeValues.length; i++) {
            Random rand2 = new Random();
            int randInt = rand2.nextInt(10);

            switch (rand2.nextInt(12)) {
                case 0:
                    break;
                case 1:
                    for (int j = 0; j < randInt; j++) {
                        Variables.HEAD_CHILD.interact("Change head");
                    }
                    break;
                case 2:
                    for (int j = 0; j < randInt; j++) {
                        Variables.JAW_CHILD.interact("Change jaw");
                    }
                    break;
                case 3:
                    for (int j = 0; j < randInt; j++) {
                        Variables.TORSO_CHILD.interact("Change torso");
                    }
                    break;
                case 4:
                    for (int j = 0; j < randInt; j++) {
                        Variables.ARMS_CHILD.interact("Change arms");
                    }
                    break;
                case 5:
                    for (int j = 0; j < randInt; j++) {
                        Variables.HANDS_CHILD.interact("Change hands");
                    }
                    break;
                case 6:
                    for (int j = 0; j < randInt; j++) {
                        Variables.LEGS_CHILD.interact("Change legs");
                    }
                    break;
                case 7:
                    for (int j = 0; j < randInt; j++) {
                        Variables.FEET_CHILD.interact("Change feet");
                    }
                    break;
                case 8:
                    for (int j = 0; j < randInt; j++) {
                        Variables.HAIR_CHILD.interact("Recolour hair");
                    }
                    break;
                case 9:
                    for (int j = 0; j < randInt; j++) {
                        Variables.TORSO_COLOUR_CHILD.interact("Recolour torso");
                    }
                    break;
                case 10:
                    for (int j = 0; j < randInt; j++) {
                        Variables.LEGS_COLOUR_CHILD.interact("Recolour legs");
                    }
                    break;
                case 11:
                    for (int j = 0; j < randInt; j++) {
                        Variables.FEET_COLOUR_CHILD.interact("Recolour feet");
                    }
                    break;
                case 12:
                    for (int j = 0; j < randInt; j++) {
                        Variables.SKIN_COLOUR_CHILD.interact("Recolour skin");
                    }
                    break;
                default:
                    break;
            }
        }
        Variables.ACCEPT_CHILD.interact("Accept");
        Time.sleepUntil(new Condition() {
                            @Override
                            public boolean check() {
                                return !Variables.MASTER_CHILD.isOnScreen();
                            }
                        },
                Random.nextInt(900, 1150)
        );


    }

    private final int LOCATION = 22;

    private final Color color1 = new Color(255, 51, 0);
    private final Color color2 = new Color(0, 0, 0);
    private final Color color3 = new Color(51, 255, 255);

    private final BasicStroke stroke1 = new BasicStroke(1);

    private final Font font1 = new Font("Calibri", 1, 16);

    public void onRepaint(Graphics g1) {
        Graphics2D g = (Graphics2D)g1;
        g.setColor(color1);
        g.fillRect(2, 3, 516, 36);
        g.setColor(color2);
        g.setStroke(stroke1);
        g.drawRect(2, 3, 516, 36);
        g.setFont(font1);
        g.drawString("State: " + currentState, 6, 27);
        g.drawString("Accounts Completed: " + accountsComplete, 266, 27);
        g.setColor(color3);
        g.drawString("Time Running: " + t.getFormattedTime() , 5, 56);
    }
    public boolean onTutorialIsland(){
        return Settings.get(LOCATION) == 33554432;

    }

    private String timeRunning()     {
        long millis;
        currentTime = System.currentTimeMillis();
        millis = currentTime - startTime;
        long second = (millis / 1000) % 60;
        long minute = (millis / (1000 * 60)) % 60;
        long hour = (millis / (1000 * 60 * 60)) % 24;
        String time = String.format("%02d:%02d:%02d", hour, minute, second);
        return time;
    }

    public static boolean AccountLog(){
        AccountCreation AC = new AccountCreation();

        if (AC.COMPLETE == "Taken"){
            LogHandler.log("Account taken, trying again");
        }
        else if (AC.COMPLETE == "Blocked"){
            LogHandler.log("Blocked from creating too many accounts, waiting then trying again");
        }
        else if (AC.COMPLETE == "Created"){
            LogHandler.log("Account successfully Created. Username = " + AC.newUsername);
        }
        else{
            LogHandler.log("unknown");
        }
        return true;
    }
}