package tutorialIsland;
import org.tbot.bot.Account;
import org.tbot.internal.handlers.LogHandler;
import org.tbot.methods.Skills;

import org.tbot.util.TEnvironment;
import tutorialIsland.mainHandler;
import java.io.*;
import java.net.*;
import java.util.Random;

/**
 * Created by Aaron on 21/06/2015.
 */
public class AccountCreation {
    public static String newEmail = "";
    public static String newUsername = "";
    public static String work = "I'm working";
    public static String COMPLETE = "Null";
    public static boolean TAKEN = false;
    public static String emailString;
    public static String passwordString;
    public static String domainString;
    public static String newPassword;

    public static void main(String email, String domain, String password, String... args) throws Exception {
        AccountDetails(email, domain, password);
    }

    public static void writeAccountToTopbot(String email, String password, String bankPin) throws Exception {
        TEnvironment.addAccount(new Account(email, password, bankPin, Skills.Skill.Cooking));
        TEnvironment.saveAccounts();
        System.out.println("Saved " + email + ":" + password);
    }

    public static void AccountDetails(String username, String domain, String password) throws Exception {
        emailString = "";
        passwordString = "";
        domainString = "";
        emailString = username;
        passwordString = password;
        domainString = domain;
        Random r = new Random();
        //RANDOMISATION
        if (newUsername == ""){
            for (int i = 0; i < (username.length()); i++) {
                if (username.charAt(i) != '#' && username.charAt(i) != '?') {
                    newUsername += username.charAt(i);
                } else if (username.charAt(i) == '?') {
                    int randomlow = r.nextInt((122 - 97) + 1) + 97;
                    int randomcap = r.nextInt((90 - 65) + 1 )+ 65;
                    int randomchoice = r.nextInt(2);
                    if (randomchoice == 1) {
                        newUsername += Character.toString((char) randomlow);
                    }
                    else if (randomchoice == 0){
                        newUsername += Character.toString((char) randomcap);
                    }
                } else if (username.charAt(i) == '#') {
                    int random = r.nextInt((57 - 48) + 1) + 48;
                    if (random >= 48 && random <= 57) {
                        newUsername += Character.toString((char) random);
                    }
                }
            }
        }
        else {
            newUsername = "";
            for (int i = 0; i < (username.length()); i++) {
                if (username.charAt(i) != '#' && username.charAt(i) != '?') {
                    newUsername += username.charAt(i);
                } else if (username.charAt(i) == '?') {
                    int randomlow = r.nextInt((122 - 97) + 1) + 97;
                    int randomcap = r.nextInt((90 - 65) + 1 )+ 65;
                    int randomchoice = r.nextInt(2);
                    if (randomchoice == 1) {
                        newUsername += Character.toString((char) randomlow);
                    }
                    else if (randomchoice == 0){
                        newUsername += Character.toString((char) randomcap);
                    }
                } else if (username.charAt(i) == '#') {
                    int random = r.nextInt((57 - 48) + 1) + 48;
                    if (random >= 48 && random <= 57) {
                        newUsername += Character.toString((char) random);
                    }
                }
            }
        }
        newEmail = newUsername + domain;
        System.out.println(newEmail);
        newPassword = password;
        CreateAccount(newEmail, newUsername, password);
    }

    public static boolean WriteToFile(String email, String password) throws Exception {
        File Accounts = new File("Accounts.txt");
        if (!Accounts.exists()) {
            Accounts.createNewFile();
            FileWriter fw = new FileWriter("Accounts.txt");
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(email + ":" + password + "\n");
            bw.close();
        } else {
            FileWriter fw = new FileWriter("Accounts.txt", true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.newLine();
            bw.write(email + ":" + password + "\n");
            bw.close();
        }
        return true;
    }

    public static boolean CreateAccount(String email, String name, String password) throws Exception {
        String SUBSTR_BLOCKED = "blocked from creating too many";
        String SUBSTR_TAKEN = "passwords you have";
        String SUBSTR_CREATED = "Click the link we have included in the confirmation email.";


        Random r = new Random();
        int age = 18 + r.nextInt(10);

        try {
            String urlParameters = "onlyOneEmail=" + URLEncoder.encode("1", "UTF-8") +
                    "&age=" + URLEncoder.encode("" + age, "UTF-8") +
                    "&displayname_preset=" + URLEncoder.encode("true", "UTF-8") +
                    "&displayname=" + URLEncoder.encode(name, "UTF-8") +
                    "&email1=" + URLEncoder.encode(email, "UTF-8") +
                    "&password1=" + URLEncoder.encode(password, "UTF-8") +
                    "&password2=" + URLEncoder.encode(password, "UTF-8") +
                    "&agree_pp_and_tac=" + URLEncoder.encode("1", "UTF-8") +
                    "&submit=" + URLEncoder.encode("Join Now", "UTF-8");

            URL AccountPage = new URL("https://secure.runescape.com/m=account-creation/g=oldscape/create_account_funnel.ws?trialactive=true");
            HttpURLConnection Conn = (HttpURLConnection) AccountPage.openConnection();
            Conn.setDoOutput(true);
            Conn.setDoInput(true);
            Conn.setRequestMethod("POST");
            Conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            Conn.setRequestProperty("charset", "utf-8");
            Conn.setRequestProperty("Content-Length", "" + Integer.toString(urlParameters.getBytes().length));
            Conn.setUseCaches(false);
            DataOutputStream Write = new DataOutputStream(Conn.getOutputStream());
            Write.writeBytes(urlParameters);
            Write.flush();
            Write.close();
            BufferedReader in = new BufferedReader((new InputStreamReader(Conn.getInputStream())));
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                if (inputLine.contains(SUBSTR_TAKEN)) {
                    LogHandler.log("Account name taken... Trying again!");
                    COMPLETE = "Taken";
                    TAKEN = true;
                    mainHandler.taken = true;
                    in.close();
                    //restart();
                    //Account name taken
                } else if (inputLine.contains(SUBSTR_BLOCKED)) {
                    System.out.println(inputLine);
                    System.out.println("Blocked from creating accounts!");
                    COMPLETE = "Blocked";
                    //Blocked for creating too many accounts
                    Thread.sleep(150000);
                } else if (inputLine.contains(SUBSTR_CREATED)) {
                    System.out.println("Account Created!");
                    COMPLETE = "Created";
                    //Account successfully created
                    String bankPin = "";
                    mainHandler.taken = false;
                    writeAccountToTopbot(email, password, bankPin);
                    WriteToFile(email, password);
                    mainHandler.LoginHandler(email, password);
                    in.close();
                    return true;
                }
            }
        }catch(IOException e){
            e.printStackTrace();
        }
        return false;
    }
}