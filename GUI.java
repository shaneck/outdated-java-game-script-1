package tutorialIsland;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Aaron on 22/06/2015.
 */


public class GUI extends JFrame {

    private static int count = 0;

    public static String emailString, domainString, passwordString;

    private static String[] Domains = {"@outlook.com", "@hotmail.com", "@hotmail.co.uk", "@gmail.com", "@live.co.uk", "@live.com", "@yahoo.com"};


    public static void run(){
        try{
            Thread.sleep(1000);
        }catch (Exception e){
            e.printStackTrace();
        }

        JFrame f = new JFrame();
        JTextField email = new JTextField("");
        JButton start = new JButton();
        JPasswordField password = new JPasswordField();
        JComboBox c = new JComboBox();
        JPanel p = new JPanel();

        f.setResizable(false);
        f.setTitle("SCRIPT");
        f.setSize(280, 180);
        f.setLocation(100, 100);
        GridLayout Layout = new GridLayout(2, 2, 10, 10);
        p.setLayout(Layout);
        p.setBorder(BorderFactory.createEmptyBorder(10, 0, 10, 10));

        JLabel emaillabel = new JLabel("Enter Email/User:", JLabel.RIGHT);
        JLabel passwordlabel = new JLabel("Enter Password:", JLabel.RIGHT);

        for (int i = 0; i < Domains.length; i++) {
            c.addItem(Domains[count++]);
        }
        password.setEchoChar('*');
        start.setText("Start!");

        p.add(emaillabel);
        p.add(email);
        p.add(c);
        p.add(passwordlabel);
        p.add(password);
        p.add(start);
        f.add(p);
        f.pack();
        f.setVisible(true);

        start.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                start.setText("Started");
                emailString = email.getText();
                domainString = c.getSelectedItem().toString();
                passwordString = password.getText();
                System.out.println(emailString + "    " + passwordString);
                Create();
                f.dispose();
            }
        });
    }

    public static void Create(){
        try {
            AccountCreation.AccountDetails(emailString, domainString, passwordString);
            mainHandler.AccountLog();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



}